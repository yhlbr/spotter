<?php

namespace App\Http\Controllers;

use App\Helpers\SpotifyHelper;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        if (!SpotifyHelper::checkToken($request)) {
            return redirect('login');
        }
        $token = $request->session()->get('access_token');

        $term = SpotifyHelper::$statsTerms[0];
        if ($request->has('term') && in_array($term, SpotifyHelper::$statsTerms)) {
            $term = $request->get('term');
        }

        $user = SpotifyHelper::getProfile($token);
        $songs = SpotifyHelper::getStats($token, 'tracks', $term);
        $songs = collect($songs['items']);

        return view('home', ['user' => $user, 'songs' => $songs, 'term' => $term]);
    }
}
