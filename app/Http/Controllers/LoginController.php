<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    public function index(Request $request)
    {
        $state = Str::random(32);
        $request->session()->put('state', $state);

        $query = [];
        $query['state'] = $state;
        $query['client_id'] = env('SPOTIFY_CLIENT_ID');
        $query['redirect_uri'] = route('redirect');
        $query['response_type'] = 'token';
        $query['scope'] = 'user-read-private user-top-read';

        $url = env('SPOTIFY_AUTH_URL') . "?" . http_build_query($query);

        return view('login', ['url' => $url]);
    }

    public function callback(Request $request)
    {
        if ($request->session()->pull('state') != $request->get('state')) {
            return redirect('login');
        }

        // Put access token
        $request->session()->put('access_token', $request->get('access_token'));
        $request->session()->put('expires_in', $request->get('expires_in'));
        $request->session()->put('time', Carbon::now()->timestamp);

        return redirect('home');
    }

    public function redirect()
    {
        return view('redirect');
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect('login');
    }
}
