<?php

namespace App\Helpers;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Request;

class SpotifyHelper
{
    public static $statsTerms = ['long_term', 'medium_term', 'short_term'];

    public static function checkToken(Request $request)
    {
        if (!$request->session()->has(['access_token', 'time', 'expires_in'])) {
            return false;
        }

        $expiringTime = (int)$request->session()->get('time') + (int)$request->session()->get('expires_in');
        if ($expiringTime < Carbon::now()->timestamp) {
            return false;
        }

        return true;
    }

    /**
     * @param $token
     * @return bool|string
     */
    public static function getProfile($token)
    {
        return self::makeRequest($token, 'me');
    }

    public static function getStats($token, $type, $time_range)
    {
        if (!in_array($type, ['artists', 'tracks'])) {
            return false;
        }

        if (!in_array($time_range, self::$statsTerms)) {
            return false;
        }

        $endpoint = 'me/top/' . $type;

        return self::makeRequest($token, $endpoint, [
            'limit' => '50',
            'time_range' => $time_range
        ]);
    }

    /**
     * @param $token
     * @param $endpoint
     * @param array $query
     * @return bool|mixed
     */
    protected static function makeRequest($token, $endpoint, $query = [])
    {
        $client = self::getClient($token);
        try {
            $response = $client->request(
                'GET',
                self::getEndpoint($endpoint),
                [
                    'query' => $query,
                    'headers' => self::getHeaders($token)
                ]
            )->getBody();
        } catch (GuzzleException $ex) {
            dd($ex->getMessage());
            return false;
        }

        return json_decode($response->getContents(), true);
    }

    /**
     * @param $token
     * @return Client
     */
    protected static function getClient($token)
    {
        static $client = null;
        if (!is_null($client)) {
            return $client;
        }

        $client = new Client([
            'headers' => self::getHeaders($token)
        ]);

        return $client;
    }

    protected static function getHeaders($token)
    {
        return [
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ];
    }

    /**
     * @param $url
     * @return string
     */
    protected static function getEndpoint($url)
    {
        return env('SPOTIFY_API_ENDPOINT') . $url;
    }
}
