@extends('master')

@push('styles')
    <link rel="stylesheet" href="{{ mix('css/home.css') }}"/>
@endpush

@push('scripts')
    <script src="//code.jquery.com/jquery.min.js"></script>
@endpush

@section('content')
    <div class="container">
        <h1>Hi
            <span class="name-container">
            <a target="_blank" href="{{ $user['external_urls']['spotify'] }}" class="name">{{ $user['display_name'] }}</a>
        </span>
        </h1>
        <a class="btn logout" href="{{ route('logout') }}">Logout</a>

        <form action="{{ route('home') }}" method="get" id="timespan">
            <div class="select-wrapper">
                <label for="timespan">Zeitspanne<br>
                    <select name="term" onchange="$('#timespan').submit();">
                        <option value="long_term" {{ $term == 'long_term' ? 'selected' : '' }}>Jahre</option>
                        <option value="medium_term" {{ $term == 'medium_term' ? 'selected' : '' }}>Monate</option>
                        <option value="short_term" {{ $term == 'short_term' ? 'selected' : '' }}>Wochen</option>
                    </select>
                </label>
            </div>
        </form>

        <h2>Deine Statistik</h2>

        <ol class="songs">
            @foreach($songs as $idx => $song)
                <li>
                    <a target="_blank"
                       href="{{ $song['artists'][0]['external_urls']['spotify'] }}">{{ $song['artists'][0]['name'] }}</a>
                    -
                    <a target="_blank" href="{{ $song['external_urls']['spotify'] }}">{{ $song['name'] }}</a>
                </li>
            @endforeach
        </ol>
    </div>
@endsection
