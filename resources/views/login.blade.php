@extends('master')

@push('styles')
<link rel="stylesheet" href="{{ mix('css/login.css') }}">
@endpush

@section('content')
<div class="content">
    <div class="title">
        <h1>Log in with <span class="text-container">Spotify</span></h1>
    </div>
    <div class="button">
        <a class="btn" href="{{ $url }}">Login</a>
    </div>
</div>
@endsection
