<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});


Route::get('/login', 'LoginController@index')->name('login');
Route::get('/redirect', 'LoginController@redirect')->name('redirect');
Route::get('/callback', 'LoginController@callback')->name('callback');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', 'LoginController@logout')->name('logout');

Route::get('/api/stats', 'StatsController@index')->name('stats');